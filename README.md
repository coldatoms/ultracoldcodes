# UltracoldCodes

## Preamble
This repository collects information about available software dedicated to studies of ultracold atomic systems (it does not provide software itself). It also provides a list of good practices, which in long term should result in better integration between various components. The repository aims in providing a community-driven software platform for exchanging and integrating open-source software related to ultracold atoms.

**Important:** We have applied for GitLab's [Solutions for Open Source Projects Program](https://about.gitlab.com/solutions/open-source/), which requires that all projects be public and have an [approved OSI](https://opensource.org/licenses/alphabetical) `LICENSE` file.  Please ensure you include this in your project.  (If you need to work in private, please do that in another namespace, then transfer it here when you are ready to release under an OSI license.)

## List of (ultracold) codes
Here we provide [the list of open-source software](./UltracoldCodes.md) dedicated to studies of ultracold atomic systems.  If you published your codes/libs/software that may be relevant for the ultracold atomic community, we encourage you to add it to the table. Use [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) functionality to do it.  
<br>
[LIST OF CODES](./UltracoldCodes.md)

## Good practices
Here we provide [Good Practices Guide](./GoodPractises.md), to help developers to improve the usability of their codes. The list is discussed and updated during the community [meetings](./Meetings.md).
Following the [Good Practices Guide](./GoodPractises.md) is not mandatory, however, we encourage contributors to implement them in their software successively.  
<br>
[GOOD PRACTICES GUIDE](./GoodPractises.md)

## Meetings
The online [meetings](./Meetings.md) are approximately once per quarter. Their aim is to integrate the community that utilizes methods of computational physics for ultracold atomic studies.  The meeting is open for everyone. If you want to take part in the meeting, please subscribe to the mailing list, by writing to [GW](http://wlazlowski.fizyka.pw.edu.pl). You can  use [Issues](https://gitlab.com/coldatoms/ultracoldcodes/-/issues) to suggest a topic that we can discuss in the next meeting.  
<br>
[LIST OF PAST AND PLANNED MEETINGS ](./Meetings.md)
